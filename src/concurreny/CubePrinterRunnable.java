package concurreny;

public class CubePrinterRunnable implements Runnable {

	private final int szam;

	public CubePrinterRunnable(int szam) {
		this.szam = szam;
	}

	@Override
	public void run() {
		for (int i = 1; i <= szam; i++) {
			System.out.println(i * i * i);
		}
	}

}
