package concurreny;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class FileWriterThread extends Thread {
	
	private	BufferedWriter output;
	
	private String text;
	
	
	
	
	public FileWriterThread(String text, String fajl ) {
		
		try {
			
			this.output=new BufferedWriter(new FileWriter(fajl, true));
			this.text= text;
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
    public FileWriterThread( ) {
		
		
	}


	@Override
	public void run() {
		try {
			output.write(text);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
