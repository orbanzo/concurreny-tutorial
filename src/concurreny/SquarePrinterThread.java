package concurreny;

public class SquarePrinterThread extends Thread {
	
	private int number;
	
	public SquarePrinterThread(int number) {
		this.number= number;
		
	}

	@Override
	public void run() {
		for (int i = 1; i < number; i++) {
			System.out.println(i*i);
		}
	
	}
	
	
}
